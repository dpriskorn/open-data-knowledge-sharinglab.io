+++
title = "Delta på workshop"
weight = 10

[asset]
  icon = "fas fa-pencil-alt"
+++

En gång i månaden anordnas en digital workshop hos Goto10. Anmälningar sker här.
