# Readme

Anvisning för att utveckla eller göra korrigeringar till nosad.se.

## Tack till

[GoHugo](gohugo.io) används som ramverk för att utveckla denna statiska startsida. Sajten använder [temat](https://syna.okkur.org/docs).

## Kom igång på fem minuter
1. Checka ut projekten
`git clone https://gitlab.com/open-data-knowledge-sharing/open-data-knowledge-sharing.gitlab.io.git`
2. [Installera GoHugo](https://gohugo.io/getting-started/quick-start/)
3. Kör sidan lokalt
`cd  https://gitlab.com/open-data-knowledge-sharing/open-data-knowledge-sharing.gitlab.io.git && hugo serve`
## Support
Kontakta [utvecklaren](https://gitlab.com/Sodjs)

----

*Code is licensed under the [Apache License, Version 2.0](/LICENSE).*  
*Documentation/examples are licensed under [Creative Commons BY-SA 4.0](/docs/LICENSE).*  

---
