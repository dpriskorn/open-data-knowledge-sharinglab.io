+++
fragment = "content"
#disabled = true
date = "2017-10-05"
weight = 100
#background = ""

title = "Om sajten"
#subtitle = ""
+++

Sajten drivs av Arbetsförmedlingen.

---

Innehållet på denna sajt, vilket Arbetsförmedlingen är ansvarig för, är licenserad med Creative Commons Attribution 4.0 International License. Personer som bidrar med innehåll till sajten har också godkänt ovan licens för innehållsbidrag om inte annat uttrycks.

### Varför ett nätverk?

Vinsterna är många; öppen innovation; kostnadseffektiv digitalisering; kompetensdelning; samverkan som ökar chansen till interoperabilitet mellan system och verksamheter.
Vi tror på att gå från Ego till Eko. Att öppet samarbeta kring vår information för att bättre kunna fullfölja vårt uppdrag. Våra kunder delas av andra aktörer inom offentlig förvaltning. Våra kunders livshändelser spänner över flertalet aktörer inom offentlig sektor, men också privat sektor. Genom att skapa förutsättningar att öppet dela information och teknik, skapas också möjligheter för innovation – inte genom att börja på ett blankt papper men genom att hämta inspiration, data och kunskap där den redan finns och förvaltas.

### Vad ska nätverket göra?

Därför vill vi med detta nätverk bjuda in till kunskapsdelning och samverkan kring hur användande och samutveckling av öppna data och tillika öppen källkod och öppna standarder kan bidra till dessa vinster. Vårt mål är att hålla en praktisk tyngdpunkt och hjälpa och inspirera myndigheter i hur de kan ta nästa steg för att öka påverkan och driva på nyttiggörandet av deras öppna data.
