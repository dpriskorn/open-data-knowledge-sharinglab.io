+++
#fragment = "header"
weight = "51"

background = "light"
title = "Katalog som alla kan bidra till"
subtitle = "[Vilken öppen programvara används inom offentlig sektor?](https://open-data-knowledge-sharing.gitlab.io/katalogen/)"
+++

[Katalog över öppen programvara inom offentlig sektor](https://gitlab.com/open-data-knowledge-sharing/wiki/-/wikis/Katalog-%C3%B6ver-%C3%B6ppen-programvara-inom-offentlig-sektor)
