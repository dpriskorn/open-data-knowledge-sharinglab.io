+++
fragment = "header"
weight = "25"

background = "light"
#title = "Om"
subtitle = "Nätverkets mål är att hålla en praktisk tyngdpunkt och hjälpa och inspirera offentlig verksamhet i hur de kan ta nästa steg för att öka tillgängliggörandet och nyttjandet av öppna data"
+++
