+++
fragment = "header"
weight = "60"

background = "light"
title = "Genomförda workshops"
subtitle = "Inspelade workshops nedan, presentationerna och sammanfattningar finns [här](https://gitlab.com/open-data-knowledge-sharing/wiki/-/wikis/Digital-Workshopserie)"
+++
