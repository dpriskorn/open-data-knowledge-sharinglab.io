+++
fragment = "hero"
date = "2016-09-07"
weight = 5
background = "light" # can influence the text color
particles = false

title = "Kunskapsdelning"
subtitle = "Kunskapsdelning öppna data och öppen källkod"

[header]
  image = "markus-winkler-UXNXItayGmo-unsplash (1) (002) editerad.jpg"

[asset]
  image = "nosad_transparent.png"
  width = "500px" # optional - will default to image width
+++
