+++
fragment = "nav"
#disabled = true
date = "2018-05-17"
weight = 0
background = "white"

[repo_button]
  url = "https://gitlab.com/open-data-knowledge-sharing/open-data-knowledge-sharing.gitlab.io/"
  text = "Sajtens källkod" # default: "Star"
  icon = "fab fa-gitlab" # defaults: "fab fa-gitlab"

# Branding options
[asset]
 image = "logo_nosad_no_text.png"
 text = "Network open source and data"
+++
