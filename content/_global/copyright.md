+++
fragment = "copyright"
#disabled = true
date = "2016-09-07"
weight = 1250
#background = ""

copyright = "Sajtens innehåll är [licenserat med CC-BY International 4.0](https://creativecommons.org/licenses/by/4.0/deed.sv)"
attribution = false # enable attribution by setting it to true
+++
