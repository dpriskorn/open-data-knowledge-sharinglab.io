+++
fragment = "footer"
disabled = true
date = "2016-09-07"
weight = 1200
#background = ""

menu_title = "Läs mer"

#[asset]
#  title = "Logo Title"
#  image = "logo_nosad_no_text.png"
#  text = "Logo Subtext 2"
#  url = "#"
+++

#### Om nätverket

Denna sidas syfte är att öka kunskapsdelningen mellan offentliga organisationer.
Alla uppmuntras till att öka återanvändningen inom det offentliga. Gärna genom att dela öppen källkod.
